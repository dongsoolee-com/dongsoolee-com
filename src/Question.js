import React, {Component} from 'react';
import './Question.scss';
import HelpOutlineIcon from '@material-ui/icons/HelpOutline'

class Question extends Component {
    render() {
        return (
            <span className="tooltip">
                <span className="tooltip__item">
                    <HelpOutlineIcon/>
                </span>
                <span className="tooltip__content">
                    <span className="tooltip__text">
                    </span>
                </span>
            </span>
        );
    }
}

export default Question;