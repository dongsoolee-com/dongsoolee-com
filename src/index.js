import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Header from './Header';
import Timeline from './Timeline';
import Question from './Question';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
    <div>
        <div className="header-section">
            <Header/>
            {/*<div>*/}
            {/*<Question/>*/}
            {/*</div>*/}
        </div>
    <div class="timeline-section">
        <Timeline/>
    </div>
    </div>,
    document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
