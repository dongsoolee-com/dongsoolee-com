import React, { Component } from 'react';
import { VerticalTimeline, VerticalTimelineElement }  from 'react-vertical-timeline-component';
import 'react-vertical-timeline-component/style.min.css';
import './Timeline.css';
import SchoolIcon from '@material-ui/icons/School';
import PersonIcon from '@material-ui/icons/Person';
import StarIcon from '@material-ui/icons/Star';
import AspectRatioIcon from '@material-ui/icons/AspectRatio';
import CodeIcon from '@material-ui/icons/Code';
import ExploreIcon from '@material-ui/icons/Explore';

class Timeline extends Component {
    render() {
        return (
            <VerticalTimeline>
                <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="2019년 3월 - 현재"
                    iconStyle={{background: 'rgb(233, 50, 143)', color: '#fff'}}
                    icon={<ExploreIcon/>}
                >
                    <h3 className="vertical-timeline-element-title">Frontend / Backend / Full Stack 웹 개발</h3>
                    <h4 className="vertical-timeline-element-subtitle">@구직중</h4>
                    <p>
                        <b>Frontend</b>
                        <ul>
                            <li>Vue.js | Nuxt.js | React</li>
                        </ul>
                    </p>
                    <p>
                        <b>Backend</b>
                        <ul>
                            <li>Node.js | Express</li>
                        </ul>
                    </p>
                    <p>
                        <b>Database</b>
                        <ul>
                            <li>MongoDB | MySQL</li>
                        </ul>
                    </p>
                    <p>
                        <b>DevOps</b>
                        <ul>
                            <li>Travis CI | GitLab Runner | Docker | Kubernetes</li>
                        </ul>
                    </p>
                </VerticalTimelineElement>
                <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="2019년 1월 - 3월"
                    iconStyle={{background: 'rgb(33, 150, 243)', color: '#fff'}}
                    icon={<CodeIcon/>}
                >
                    <h3 className="vertical-timeline-element-title">Frontend 개발</h3>
                    <h4 className="vertical-timeline-element-subtitle">@Founding : P2P 크라우드 펀딩 플랫폼</h4>
                    <p>
                        Vue.js | Nuxt.js | Node.js | Docker | Git | Kubernetes
                    </p>
                    <p>
                        <b>소개</b>
                        <ul>
                            <li>
                                <p style={{fontSize: '14px', color: '#666'}}>
                                    부동산 P2P 크라우드 펀딩 웹 사이트를 개발하였고, Vue.js(Nuxt.js)를 사용하였습니다.
                                </p>
                            </li>
                        </ul>
                    </p>
                    <p>
                        <b>참고 자료</b>
                        <ul>
                            <li><a href="https://founding.dongsoolee.com" target="_blank">데모 사이트(직접 구현한 부분)</a></li>
                        </ul>
                    </p>
                </VerticalTimelineElement>
                <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="2018년 9월 - 12월"
                    iconStyle={{background: 'rgb(33, 150, 243)', color: '#fff'}}
                    icon={<CodeIcon/>}
                >
                    <h3 className="vertical-timeline-element-title">Full Stack 개발</h3>
                    <h4 className="vertical-timeline-element-subtitle">@Bitrading : 암호화폐 자동매매 시스템</h4>

                    <p>
                        Vue.js | Nuxt.js | Node.js | Express | SQL | MongoDB | Docker | Git
                    </p>
                    <p>
                        <b>소개</b>
                        <ul>
                            <li>
                                <p style={{fontSize: '14px', color: '#666'}}>
                                암호화폐 거래소 업비트의 OpenAPI를 이용하여 암호화폐의 매수/매도/손절매 등을 자동화 하는 웹 서비스입니다.
                                실시간으로 거래소 시장 정보를 업데이트하며 업비트 서버에 초당 5~10번의 주문 요청이 가능합니다. <br /><br />
                                프론트엔드는 Vue.js(Nuxt.js)로, 백엔드는 Node.js(Express)로 개발하였습니다.
                                </p>
                            </li>
                        </ul>
                    </p>
                    <p>
                        <b>참고 자료</b>
                        <ul>
                            <li><a href="https://bitrading.kr" target="_blank">
                                웹 사이트 (현재 서비스 중단상태, 사이트는 유지 중)
                            </a></li>
                            <li><a href="https://gitlab.com/bitrading-kr/bitrading-kr/blob/master/docs/Demo.md" target="_blank">
                                시연 문서 (실제 서비스 시연)
                            </a></li>
                            <li><a href="https://gitlab.com/bitrading-kr/bitrading-kr" target="_blank">
                                프로젝트 소스코드 (GitLab)
                            </a></li>
                        </ul>
                    </p>
                </VerticalTimelineElement>
                <VerticalTimelineElement
                    className="vertical-timeline-element--work"
                    date="2016년 - 2019년"
                    iconStyle={{background: 'rgb(133, 150, 243)', color: '#fff'}}
                    icon={<PersonIcon />}
                >
                    <h3 className="vertical-timeline-element-title">개인 프로젝트</h3>
                    <h4 className="vertical-timeline-element-subtitle"><a href="https://github.com/mrlee23" target="_blank">
                        @GitHub
                    </a></h4>

                    <p>
                        <ul>
                            <li>
                                통계 프로젝트
                                <ul>
                                    <li>
                                        <a href="https://github.com/mrlee23/KoreanSentimentAnalyzer" target="_blank">
                                            한국어 감성 분석기
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://github.com/mrlee23/bankruptcy-prediction" target="_blank">
                                            통계적 학습 방법을 이용한 기업의 파산 가능성 예측
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                Static 웹 페이지 / 문서화
                                <ul>
                                    <li>
                                        <a href="https://github.com/mrlee23/Linux-Command.org" target="_blank">
                                            Linux-Command.org
                                        </a>
                                    </li>
                                    <li>
                                        <a href="https://github.com/mrlee23/Development-Notes" target="_blank">
                                            개발 노트
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                </VerticalTimelineElement>
                <VerticalTimelineElement
                    className="vertical-timeline-element--education"
                    date="2014년 - 2015년"
                    iconStyle={{background: 'rgb(93, 189, 30)', color: '#fff'}}
                    icon={<AspectRatioIcon/>}
                >
                    <h3 className="vertical-timeline-element-title">자격증 취득</h3>
                    <p>
                        리눅스 마스터 1급
                    </p>
                    <p>
                        네트워크 관리사 2급
                    </p>
                </VerticalTimelineElement>
                <VerticalTimelineElement
                    className="vertical-timeline-element--education"
                    date="2012년 - 2019년"
                    iconStyle={{background: '#872434', color: '#fff'}}
                    icon={<SchoolIcon/>}
                >
                    <h3 className="vertical-timeline-element-title">고려대학교 사회체육학과 진학</h3>
                    <h4 className="vertical-timeline-element-subtitle">학위</h4>
                    <p>
                        <b> 주 전공</b> 국제스포츠 학부 스포츠과학 전공
                    </p>
                    <p>
                        <b> 이중 전공 </b> 컴퓨터 융합 소프트웨어 학과
                    </p>
                </VerticalTimelineElement>
                <VerticalTimelineElement
                    iconStyle={{background: 'rgb(16, 204, 82)', color: '#fff'}}
                    icon={<StarIcon/>}
                />
            </VerticalTimeline>
        );
    }
}


export default Timeline;
