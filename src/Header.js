import React, { Component } from 'react';
import './Header.css';

class Header extends Component {
    render() {
        return (
            <div class="header">
                <div class="header__bg">
                    <div className="header__title">
                        <h1>Dongsoo Lee</h1>
                        <h1>이동수</h1>
                    </div>
                </div>
                <div class="header__bottom">
                </div>
            </div>
        );
    }
}


export default Header;
